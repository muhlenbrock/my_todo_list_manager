﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MyTodo
{
    static class Settings
    {
        public static SettingsSet Set = new SettingsSet();
        public static string currentFile = "";
    }

    public class SettingsSet
    {
        public bool ShowDone = false;
        public string LastSavedFile = string.Empty;
        public Color Col_LOW = Color.Black;
        public Color Col_MED = Color.Blue;
        public Color Col_High = Color.Red;
        public bool Annoy = false;
        public int AnnoyMinutes = 5;
        public int AnnoyDeadline = 3;

        public SettingsSet()
        {

        }
    }
}
