﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyTodo
{
    public partial class dlg_settings : Form
    {
        public dlg_settings()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Label lb = sender as Label;
            if (colorDialog1.ShowDialog()==DialogResult.OK)
            {
                lb.ForeColor = colorDialog1.Color;
                
            }
        }

        private void dlg_settings_Load(object sender, EventArgs e)
        {
            LOW.ForeColor = Settings.Set.Col_LOW;
            MED.ForeColor = Settings.Set.Col_MED;
            HIGH.ForeColor = Settings.Set.Col_High;

            cb_annoyme.Checked = Settings.Set.Annoy;
            nb_minutes.Value = Settings.Set.AnnoyMinutes;
            nb_daystogo.Value = Settings.Set.AnnoyDeadline;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            Settings.Set.Col_LOW = LOW.ForeColor;
            Settings.Set.Col_MED = MED.ForeColor;
            Settings.Set.Col_High = HIGH.ForeColor;

            Settings.Set.Annoy = cb_annoyme.Checked;
            Settings.Set.AnnoyMinutes = (int)nb_minutes.Value;
            Settings.Set.AnnoyDeadline = (int)nb_daystogo.Value;
        }
    }
}
