﻿namespace MyTodo
{
    partial class Main_Window
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.taskbox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tb_comment = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tb_topic = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn_setdone = new System.Windows.Forms.Button();
            this.rb_undone = new System.Windows.Forms.RadioButton();
            this.rb_done = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rb_sortbyPRIO = new System.Windows.Forms.RadioButton();
            this.btn_setlimit = new System.Windows.Forms.Button();
            this.cb_setlimit = new System.Windows.Forms.CheckBox();
            this.picklimit = new System.Windows.Forms.DateTimePicker();
            this.cb_showdone = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.addbox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.listeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernUnterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zurücksetzenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ladenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einträgeHinzufügenSTRGSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gelöschteEinträgeRettenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setzePrioAufToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lOWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mEDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hIGHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markiereAlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.erledigtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unerledigtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dlg_save = new System.Windows.Forms.SaveFileDialog();
            this.dlg_open = new System.Windows.Forms.OpenFileDialog();
            this.Annoyer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // taskbox
            // 
            this.taskbox.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.taskbox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.taskbox.Dock = System.Windows.Forms.DockStyle.Left;
            this.taskbox.FullRowSelect = true;
            this.taskbox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.taskbox.HideSelection = false;
            this.taskbox.Location = new System.Drawing.Point(0, 24);
            this.taskbox.Name = "taskbox";
            this.taskbox.Size = new System.Drawing.Size(450, 647);
            this.taskbox.TabIndex = 0;
            this.taskbox.UseCompatibleStateImageBehavior = false;
            this.taskbox.View = System.Windows.Forms.View.Details;
            this.taskbox.ItemActivate += new System.EventHandler(this.taskbox_ItemActivate);
            this.taskbox.SelectedIndexChanged += new System.EventHandler(this.taskbox_SelectedIndexChanged);
            this.taskbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_Window_KeyDown);
            this.taskbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.taskbox_MouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Erl.";
            this.columnHeader1.Width = 42;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Aufgabe";
            this.columnHeader2.Width = 284;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Prio";
            this.columnHeader3.Width = 43;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Frist";
            this.columnHeader4.Width = 84;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(450, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(540, 647);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 228F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 206);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.76802F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 441);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(306, 435);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Details der Aufgabe:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.groupBox6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(300, 416);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tb_comment);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 211);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(294, 202);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Kommentar";
            // 
            // tb_comment
            // 
            this.tb_comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_comment.Location = new System.Drawing.Point(3, 16);
            this.tb_comment.Multiline = true;
            this.tb_comment.Name = "tb_comment";
            this.tb_comment.Size = new System.Drawing.Size(288, 183);
            this.tb_comment.TabIndex = 1;
            this.tb_comment.Tag = "comment";
            this.tb_comment.TextChanged += new System.EventHandler(this.tb_comment_TextChanged);
            this.tb_comment.Leave += new System.EventHandler(this.tb_comment_Leave);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tb_topic);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(294, 202);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Thema";
            // 
            // tb_topic
            // 
            this.tb_topic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_topic.Location = new System.Drawing.Point(3, 16);
            this.tb_topic.Multiline = true;
            this.tb_topic.Name = "tb_topic";
            this.tb_topic.Size = new System.Drawing.Size(288, 183);
            this.tb_topic.TabIndex = 1;
            this.tb_topic.Tag = "topic";
            this.tb_topic.TextChanged += new System.EventHandler(this.tb_topic_TextChanged);
            this.tb_topic.Leave += new System.EventHandler(this.tb_comment_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox7);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.btn_setlimit);
            this.groupBox3.Controls.Add(this.cb_setlimit);
            this.groupBox3.Controls.Add(this.picklimit);
            this.groupBox3.Controls.Add(this.cb_showdone);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(315, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(222, 435);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Aktionen";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn_setdone);
            this.groupBox7.Controls.Add(this.rb_undone);
            this.groupBox7.Controls.Add(this.rb_done);
            this.groupBox7.Location = new System.Drawing.Point(6, 232);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(196, 102);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Gewählte Einträge als...";
            // 
            // btn_setdone
            // 
            this.btn_setdone.Location = new System.Drawing.Point(3, 62);
            this.btn_setdone.Name = "btn_setdone";
            this.btn_setdone.Size = new System.Drawing.Size(132, 23);
            this.btn_setdone.TabIndex = 4;
            this.btn_setdone.Text = "...markieren";
            this.btn_setdone.UseVisualStyleBackColor = true;
            this.btn_setdone.Click += new System.EventHandler(this.btn_setdone_Click);
            // 
            // rb_undone
            // 
            this.rb_undone.AutoSize = true;
            this.rb_undone.Location = new System.Drawing.Point(3, 39);
            this.rb_undone.Name = "rb_undone";
            this.rb_undone.Size = new System.Drawing.Size(71, 17);
            this.rb_undone.TabIndex = 1;
            this.rb_undone.Tag = "Frist";
            this.rb_undone.Text = "unerledigt";
            this.rb_undone.UseVisualStyleBackColor = true;
            // 
            // rb_done
            // 
            this.rb_done.AutoSize = true;
            this.rb_done.Checked = true;
            this.rb_done.Location = new System.Drawing.Point(3, 16);
            this.rb_done.Name = "rb_done";
            this.rb_done.Size = new System.Drawing.Size(59, 17);
            this.rb_done.TabIndex = 0;
            this.rb_done.TabStop = true;
            this.rb_done.Tag = "Prio";
            this.rb_done.Text = "erledigt";
            this.rb_done.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButton3);
            this.groupBox4.Controls.Add(this.radioButton2);
            this.groupBox4.Controls.Add(this.rb_sortbyPRIO);
            this.groupBox4.Location = new System.Drawing.Point(6, 124);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(196, 102);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sortiere nach...";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(3, 62);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(60, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "Erl";
            this.radioButton3.Text = "Erledigt";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(3, 39);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(44, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "Frist";
            this.radioButton2.Text = "Frist";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rb_sortbyPRIO
            // 
            this.rb_sortbyPRIO.AutoSize = true;
            this.rb_sortbyPRIO.Location = new System.Drawing.Point(3, 16);
            this.rb_sortbyPRIO.Name = "rb_sortbyPRIO";
            this.rb_sortbyPRIO.Size = new System.Drawing.Size(43, 17);
            this.rb_sortbyPRIO.TabIndex = 0;
            this.rb_sortbyPRIO.TabStop = true;
            this.rb_sortbyPRIO.Tag = "Prio";
            this.rb_sortbyPRIO.Text = "Prio";
            this.rb_sortbyPRIO.UseVisualStyleBackColor = true;
            this.rb_sortbyPRIO.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // btn_setlimit
            // 
            this.btn_setlimit.Location = new System.Drawing.Point(3, 95);
            this.btn_setlimit.Name = "btn_setlimit";
            this.btn_setlimit.Size = new System.Drawing.Size(199, 23);
            this.btn_setlimit.TabIndex = 3;
            this.btn_setlimit.Text = "Frist für gewählte Einträge setzen";
            this.btn_setlimit.UseVisualStyleBackColor = true;
            this.btn_setlimit.Click += new System.EventHandler(this.btn_setlimit_Click);
            // 
            // cb_setlimit
            // 
            this.cb_setlimit.AutoSize = true;
            this.cb_setlimit.Location = new System.Drawing.Point(3, 46);
            this.cb_setlimit.Name = "cb_setlimit";
            this.cb_setlimit.Size = new System.Drawing.Size(154, 17);
            this.cb_setlimit.TabIndex = 2;
            this.cb_setlimit.Text = "Frist für alle neuen Einträge";
            this.cb_setlimit.UseVisualStyleBackColor = true;
            // 
            // picklimit
            // 
            this.picklimit.Location = new System.Drawing.Point(3, 69);
            this.picklimit.Name = "picklimit";
            this.picklimit.Size = new System.Drawing.Size(199, 20);
            this.picklimit.TabIndex = 1;
            // 
            // cb_showdone
            // 
            this.cb_showdone.AutoSize = true;
            this.cb_showdone.Location = new System.Drawing.Point(3, 16);
            this.cb_showdone.Name = "cb_showdone";
            this.cb_showdone.Size = new System.Drawing.Size(162, 17);
            this.cb_showdone.TabIndex = 0;
            this.cb_showdone.Text = "Erledigte Aufgaben anzeigen";
            this.cb_showdone.UseVisualStyleBackColor = true;
            this.cb_showdone.CheckedChanged += new System.EventHandler(this.cb_showdone_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addbox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(540, 206);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hinzufügen: (Shortcut: STRG+ALT+links)";
            // 
            // addbox
            // 
            this.addbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addbox.Location = new System.Drawing.Point(3, 16);
            this.addbox.Multiline = true;
            this.addbox.Name = "addbox";
            this.addbox.Size = new System.Drawing.Size(534, 187);
            this.addbox.TabIndex = 0;
            this.addbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.addbox_KeyDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeToolStripMenuItem,
            this.speichernToolStripMenuItem,
            this.ladenToolStripMenuItem,
            this.einstellungenToolStripMenuItem,
            this.einträgeHinzufügenSTRGSToolStripMenuItem,
            this.gelöschteEinträgeRettenToolStripMenuItem,
            this.setzePrioAufToolStripMenuItem,
            this.markiereAlsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(990, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // listeToolStripMenuItem
            // 
            this.listeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.speichernUnterToolStripMenuItem,
            this.zurücksetzenToolStripMenuItem});
            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.listeToolStripMenuItem.Text = "Liste...";
            // 
            // speichernUnterToolStripMenuItem
            // 
            this.speichernUnterToolStripMenuItem.Name = "speichernUnterToolStripMenuItem";
            this.speichernUnterToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.speichernUnterToolStripMenuItem.Text = "Speichern unter...";
            this.speichernUnterToolStripMenuItem.Click += new System.EventHandler(this.speichernUnterToolStripMenuItem_Click);
            // 
            // zurücksetzenToolStripMenuItem
            // 
            this.zurücksetzenToolStripMenuItem.Name = "zurücksetzenToolStripMenuItem";
            this.zurücksetzenToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.zurücksetzenToolStripMenuItem.Text = "Zurücksetzen";
            this.zurücksetzenToolStripMenuItem.Click += new System.EventHandler(this.zurücksetzenToolStripMenuItem_Click);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.speichernToolStripMenuItem.Text = "Speichern";
            this.speichernToolStripMenuItem.Click += new System.EventHandler(this.speichernToolStripMenuItem_Click);
            // 
            // ladenToolStripMenuItem
            // 
            this.ladenToolStripMenuItem.Name = "ladenToolStripMenuItem";
            this.ladenToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.ladenToolStripMenuItem.Text = "Laden";
            this.ladenToolStripMenuItem.Click += new System.EventHandler(this.ladenToolStripMenuItem_Click);
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // einträgeHinzufügenSTRGSToolStripMenuItem
            // 
            this.einträgeHinzufügenSTRGSToolStripMenuItem.Name = "einträgeHinzufügenSTRGSToolStripMenuItem";
            this.einträgeHinzufügenSTRGSToolStripMenuItem.Size = new System.Drawing.Size(127, 20);
            this.einträgeHinzufügenSTRGSToolStripMenuItem.Text = "Einträge Hinzufügen";
            this.einträgeHinzufügenSTRGSToolStripMenuItem.Click += new System.EventHandler(this.einträgeHinzufügenSTRGSToolStripMenuItem_Click);
            // 
            // gelöschteEinträgeRettenToolStripMenuItem
            // 
            this.gelöschteEinträgeRettenToolStripMenuItem.Name = "gelöschteEinträgeRettenToolStripMenuItem";
            this.gelöschteEinträgeRettenToolStripMenuItem.Size = new System.Drawing.Size(151, 20);
            this.gelöschteEinträgeRettenToolStripMenuItem.Text = "Gelöschte Einträge retten";
            this.gelöschteEinträgeRettenToolStripMenuItem.Click += new System.EventHandler(this.gelöschteEinträgeRettenToolStripMenuItem_Click);
            // 
            // setzePrioAufToolStripMenuItem
            // 
            this.setzePrioAufToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lOWToolStripMenuItem,
            this.mEDToolStripMenuItem,
            this.hIGHToolStripMenuItem});
            this.setzePrioAufToolStripMenuItem.Name = "setzePrioAufToolStripMenuItem";
            this.setzePrioAufToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.setzePrioAufToolStripMenuItem.Text = "Setze Prio auf...";
            // 
            // lOWToolStripMenuItem
            // 
            this.lOWToolStripMenuItem.Name = "lOWToolStripMenuItem";
            this.lOWToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.lOWToolStripMenuItem.Tag = "0";
            this.lOWToolStripMenuItem.Text = "LOW";
            this.lOWToolStripMenuItem.Click += new System.EventHandler(this.lOWToolStripMenuItem_Click);
            // 
            // mEDToolStripMenuItem
            // 
            this.mEDToolStripMenuItem.Name = "mEDToolStripMenuItem";
            this.mEDToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.mEDToolStripMenuItem.Tag = "1";
            this.mEDToolStripMenuItem.Text = "MED";
            this.mEDToolStripMenuItem.Click += new System.EventHandler(this.lOWToolStripMenuItem_Click);
            // 
            // hIGHToolStripMenuItem
            // 
            this.hIGHToolStripMenuItem.Name = "hIGHToolStripMenuItem";
            this.hIGHToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.hIGHToolStripMenuItem.Tag = "2";
            this.hIGHToolStripMenuItem.Text = "HIGH";
            this.hIGHToolStripMenuItem.Click += new System.EventHandler(this.lOWToolStripMenuItem_Click);
            // 
            // markiereAlsToolStripMenuItem
            // 
            this.markiereAlsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.erledigtToolStripMenuItem,
            this.unerledigtToolStripMenuItem});
            this.markiereAlsToolStripMenuItem.Name = "markiereAlsToolStripMenuItem";
            this.markiereAlsToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.markiereAlsToolStripMenuItem.Text = "Markiere als...";
            // 
            // erledigtToolStripMenuItem
            // 
            this.erledigtToolStripMenuItem.Name = "erledigtToolStripMenuItem";
            this.erledigtToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.erledigtToolStripMenuItem.Tag = "rb_done";
            this.erledigtToolStripMenuItem.Text = "erledigt";
            this.erledigtToolStripMenuItem.Click += new System.EventHandler(this.erledigtToolStripMenuItem_Click);
            // 
            // unerledigtToolStripMenuItem
            // 
            this.unerledigtToolStripMenuItem.Name = "unerledigtToolStripMenuItem";
            this.unerledigtToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.unerledigtToolStripMenuItem.Tag = "rb_undone";
            this.unerledigtToolStripMenuItem.Text = "unerledigt";
            this.unerledigtToolStripMenuItem.Click += new System.EventHandler(this.erledigtToolStripMenuItem_Click);
            // 
            // dlg_save
            // 
            this.dlg_save.Filter = "ToDo Listen (*.tdl)|*.tdl";
            // 
            // dlg_open
            // 
            this.dlg_open.FileName = "openFileDialog1";
            // 
            // Annoyer
            // 
            this.Annoyer.Tick += new System.EventHandler(this.Annoyer_Tick);
            // 
            // Main_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 671);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.taskbox);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Main_Window";
            this.Text = "ToDo-Liste";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_Window_FormClosing);
            this.Load += new System.EventHandler(this.Main_Window_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_Window_KeyDown);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView taskbox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox addbox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ladenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einträgeHinzufügenSTRGSToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cb_showdone;
        private System.Windows.Forms.Button btn_setlimit;
        private System.Windows.Forms.CheckBox cb_setlimit;
        private System.Windows.Forms.DateTimePicker picklimit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rb_sortbyPRIO;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tb_comment;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tb_topic;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn_setdone;
        private System.Windows.Forms.RadioButton rb_undone;
        private System.Windows.Forms.RadioButton rb_done;
        private System.Windows.Forms.ToolStripMenuItem gelöschteEinträgeRettenToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog dlg_save;
        private System.Windows.Forms.ToolStripMenuItem setzePrioAufToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lOWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mEDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hIGHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markiereAlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem erledigtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unerledigtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernUnterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zurücksetzenToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog dlg_open;
        private System.Windows.Forms.Timer Annoyer;
    }
}

