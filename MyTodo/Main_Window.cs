﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;

namespace MyTodo
{
    public partial class Main_Window : Form
    {
        List<ThingToDo> ToDoList = new List<ThingToDo>();
        public Main_Window()
        {
            InitializeComponent();
            erledigtToolStripMenuItem.Tag = rb_done;
            unerledigtToolStripMenuItem.Tag = rb_undone;
        }

        private void RefreshTasks()
        {
            taskbox.Items.Clear();
            foreach (ThingToDo thing in ToDoList)
            {
                if (!(thing.Done && !Settings.Set.ShowDone))
                {
                    thing.Represent(taskbox);
                }
            }
        }

        private void AddEntries()
        {
            foreach (string line in addbox.Lines)
            {
                if (line.Length > 0)
                {
                    ThingToDo Task = new ThingToDo();

                    //Prio und Topic
                    if (line.StartsWith("MED "))
                    {
                        Task.Prio = ToDoPrio.MED;
                        Task.Topic = line.Substring(4);
                    }
                    else if (line.StartsWith("HIGH "))
                    {
                        Task.Prio = ToDoPrio.HIGH;
                        Task.Topic = line.Substring(5);
                    }
                    else
                    {
                        Task.Prio = ToDoPrio.LOW;
                        Task.Topic = line;
                    }

                    //Frist wenn gewünscht
                    Task.DoBefore = cb_setlimit.Checked ? picklimit.Value : DateTime.Today.AddDays(7);

                    ToDoList.Add(Task);
                }
            }
            RefreshTasks();
            addbox.Text = "";
        }

        private void addbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.Alt && e.KeyCode==Keys.Left)
            {
                e.SuppressKeyPress = true;
                AddEntries();                
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void einträgeHinzufügenSTRGSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddEntries();
        }

        private void taskbox_ItemChecked(object sender, ItemCheckedEventArgs e)
        {

        }

        private void cb_showdone_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set.ShowDone = cb_showdone.Checked;
            RefreshTasks();
        }

        private void Main_Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                RefreshTasks();
            }
            if (e.KeyCode == Keys.Delete && taskbox.Focused && taskbox.SelectedItems.Count > 0)
            {
                foreach (ListViewItem i in taskbox.SelectedItems)
                {
                    ThingToDo todo = i.Tag as ThingToDo;
                    todo.Delete();
                }
                RefreshTasks();
            }
        }

        private void btn_setlimit_Click(object sender, EventArgs e)
        {
            if (taskbox.SelectedItems.Count > 0)
            {
                foreach (ListViewItem i in taskbox.SelectedItems)
                {
                    if (i.SubItems.Count == 3)
                    {
                        i.SubItems.Add(picklimit.Value.ToShortDateString());
                    }
                    else
                    {
                        i.SubItems[3].Text = picklimit.Value.ToShortDateString();
                    }
                    ((ThingToDo)i.Tag).DoBefore = picklimit.Value;
                }
                RefreshTasks();
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            List<ThingToDo> templist = ToDoList;

            switch (rb.Tag.ToString())
            {                
                case "Prio": ToDoList = templist.OrderByDescending(t => t.Prio).ToList();                    
                    break;
                case "Frist": ToDoList = templist.OrderBy(t => t.DoBefore).ToList();
                    break;
                case "Erl": ToDoList = templist.OrderBy(t => t.Done).ToList();
                    break;
                default:
                    break;
            }
            RefreshTasks();
        }

        private void taskbox_SelectedIndexChanged(object sender, EventArgs e)
        {            
            tb_comment.Enabled = taskbox.SelectedItems.Count == 1;
            tb_topic.Enabled   = taskbox.SelectedItems.Count == 1;   
            
            if (taskbox.SelectedItems.Count == 1)
            {
                ThingToDo Todo = taskbox.SelectedItems[0].Tag as ThingToDo;
                tb_topic.Text = Todo.Topic;
                tb_comment.Text = Todo.Comment;
            }
            else
            {
                tb_topic.Text = "";
                tb_comment.Text = "";
            }

            setzePrioAufToolStripMenuItem.Enabled = taskbox.SelectedItems.Count > 0;
            markiereAlsToolStripMenuItem.Enabled = taskbox.SelectedItems.Count > 0;
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            //unbenutzt

        }

        private void btn_setdone_Click(object sender, EventArgs e)
        {
            if (taskbox.SelectedItems.Count > 0)
            {
                foreach (ListViewItem i in taskbox.SelectedItems)
                {
                    ((ThingToDo)i.Tag).Done = rb_done.Checked;
                }
            }
            RefreshTasks();
        }

        private void tb_comment_TextChanged(object sender, EventArgs e)
        {
            if (tb_comment.Focus() && taskbox.SelectedItems.Count == 1)
            {
                ThingToDo Todo = taskbox.SelectedItems[0].Tag as ThingToDo;
                //Todo.Topic = tb_topic.Text;
                Todo.Comment = tb_comment.Text;
            }
        }

        private void tb_comment_Leave(object sender, EventArgs e)
        {
            RefreshTasks();
        }

        private void gelöschteEinträgeRettenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ThingToDo Todo in ToDoList)
            {
                Todo.Rescue();
            }
            RefreshTasks();
        }

        private void Main_Window_Load(object sender, EventArgs e)
        {
            if (File.Exists("settings.json"))
            {
                string settingsfile = File.ReadAllText("settings.json");
                dynamic SettingsFromFile = JsonConvert.DeserializeObject(settingsfile);
                try
                {
                    Settings.Set.ShowDone = (bool)SettingsFromFile.ShowDone;
                    Settings.Set.LastSavedFile = SettingsFromFile.LastSavedFile;

                    Settings.Set.Col_LOW = SettingsFromFile.Col_LOW;
                    Settings.Set.Col_MED = SettingsFromFile.Col_MED;
                    Settings.Set.Col_High = SettingsFromFile.Col_High;

                    Settings.Set.Annoy = (bool)SettingsFromFile.Annoy;
                    Settings.Set.AnnoyDeadline = Convert.ToInt32(SettingsFromFile.AnnoyDeadline);
                    Settings.Set.AnnoyMinutes = Convert.ToInt32(SettingsFromFile.AnnoyMinutes);
                }
                catch
                {
                    Debug.WriteLine("Einige Einstellungen wurden nicht geladen.");   
                }

                cb_showdone.Checked = Settings.Set.ShowDone;

                Annoyer.Interval = Settings.Set.AnnoyMinutes * 60000;
                Annoyer.Enabled = Settings.Set.Annoy;
            }
            LoadLastTodo();
        }

        private void DoAnnoy()
        {
            foreach (ThingToDo thing in ToDoList)
            {
                if (thing.WillAnnoy() && thing.DoBefore < DateTime.Today.AddDays(Settings.Set.AnnoyDeadline) && StopAnnoy(thing))
                {
                    thing.ShutUp();
                }
            }
        }

        private bool StopAnnoy(ThingToDo thing)
        {
            return MessageBox.Show("Die Aufgabe \"" + thing.Topic + "\" erfordert Deine Aufmerksamkeit!", "ACHTUNG!", MessageBoxButtons.OKCancel) == DialogResult.Cancel;
        }

        private void Main_Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            string settingstofile = JsonConvert.SerializeObject(Settings.Set);
            File.WriteAllText("settings.json", settingstofile);
            ListToFile(Settings.currentFile);
        }

        private void ListToFile(string Filename)
        {
            List<ThingToDo> Saveme = new List<ThingToDo>();
            foreach (ThingToDo Todo in ToDoList)
            {
                if (!Todo.ToBeDeleted)
                {
                    Saveme.Add(Todo);
                }
            }
            string content = JsonConvert.SerializeObject(Saveme);
            File.WriteAllText(Filename, content);
        }

        private void speichernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Settings.currentFile == "")
            {
                if (dlg_save.ShowDialog() == DialogResult.OK)
                {
                    Settings.currentFile = dlg_save.FileName;
                    ListToFile(Settings.currentFile);
                    Settings.Set.LastSavedFile = Settings.currentFile;
                }
            }
            else
            {
                ListToFile(Settings.currentFile);
            }

            
        }

        private void LoadLastTodo()
        {
            if (Settings.Set.LastSavedFile.Length > 0)
            {
                ToDoList.Clear();
                string contents = File.ReadAllText(Settings.Set.LastSavedFile);
                dynamic Data = JsonConvert.DeserializeObject(contents);
                for (int i = 0; i < Data.Count; i++)
                {
                    ThingToDo thingy = new ThingToDo();
                    thingy.Comment = Data[i].Comment;
                    thingy.DoBefore = (DateTime)Data[i].DoBefore;
                    thingy.Done = (bool)Data[i].Done;
                    thingy.Prio = (int)Data[i].Prio;
                    thingy.Topic = Data[i].Topic;
                    ToDoList.Add(thingy);
                }
                RefreshTasks();
                Settings.currentFile = Settings.Set.LastSavedFile;
            }
        }

        private void lOWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem men_item = sender as ToolStripMenuItem;
            int prio = Convert.ToInt32(men_item.Tag);

            foreach (ListViewItem item in taskbox.SelectedItems)
            {
                ((ThingToDo)item.Tag).Prio = prio;
            }
            if (rb_sortbyPRIO.Checked)
            {
                radioButton2_CheckedChanged(rb_sortbyPRIO, e);
            }
            else
            {
                RefreshTasks();
            }
        }

        private void erledigtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RadioButton rb = ((ToolStripMenuItem)sender).Tag as RadioButton;
            rb.Checked = true;
            btn_setdone_Click(btn_setdone, e);
        }

        private void speichernUnterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dlg_save.ShowDialog() == DialogResult.OK)
            {
                Settings.currentFile = dlg_save.FileName;
                ListToFile(Settings.currentFile);
                Settings.Set.LastSavedFile = Settings.currentFile;
            }
        }

        private void ladenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dlg_open.ShowDialog() == DialogResult.OK)
            {
                Settings.Set.LastSavedFile = dlg_open.FileName;
                LoadLastTodo();
            }
        }

        private void zurücksetzenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToDoList.Clear();
            RefreshTasks();
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dlg_settings SettingsBox = new dlg_settings();
            if (SettingsBox.ShowDialog() == DialogResult.OK)
            {
                RefreshTasks();
                Annoyer.Interval = Settings.Set.AnnoyMinutes * 60000;
                Annoyer.Enabled = Settings.Set.Annoy;
            }
        }

        private void tb_topic_TextChanged(object sender, EventArgs e)
        {
            if (tb_topic.Focus() && taskbox.SelectedItems.Count == 1)
            {
                ThingToDo Todo = taskbox.SelectedItems[0].Tag as ThingToDo;
                Todo.Topic = tb_topic.Text;
                //Todo.Comment = tb_comment.Text;
            }
        }

        private void taskbox_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void taskbox_ItemActivate(object sender, EventArgs e)
        {

        }

        private void Annoyer_Tick(object sender, EventArgs e)
        {
            DoAnnoy();
        }
    }
}
