﻿namespace MyTodo
{
    partial class dlg_settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.HIGH = new System.Windows.Forms.Label();
            this.MED = new System.Windows.Forms.Label();
            this.LOW = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.gb_colors = new System.Windows.Forms.GroupBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_annoyme = new System.Windows.Forms.CheckBox();
            this.nb_minutes = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nb_daystogo = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.gb_colors.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_daystogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Farbe für ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Farbe für ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Farbe für ";
            // 
            // HIGH
            // 
            this.HIGH.AutoSize = true;
            this.HIGH.BackColor = System.Drawing.Color.White;
            this.HIGH.Location = new System.Drawing.Point(64, 82);
            this.HIGH.Name = "HIGH";
            this.HIGH.Size = new System.Drawing.Size(34, 13);
            this.HIGH.TabIndex = 5;
            this.HIGH.Text = "HIGH";
            this.HIGH.Click += new System.EventHandler(this.label6_Click);
            // 
            // MED
            // 
            this.MED.AutoSize = true;
            this.MED.BackColor = System.Drawing.Color.White;
            this.MED.Location = new System.Drawing.Point(64, 53);
            this.MED.Name = "MED";
            this.MED.Size = new System.Drawing.Size(31, 13);
            this.MED.TabIndex = 4;
            this.MED.Text = "MED";
            this.MED.Click += new System.EventHandler(this.label6_Click);
            // 
            // LOW
            // 
            this.LOW.AutoSize = true;
            this.LOW.BackColor = System.Drawing.Color.White;
            this.LOW.Location = new System.Drawing.Point(64, 25);
            this.LOW.Name = "LOW";
            this.LOW.Size = new System.Drawing.Size(32, 13);
            this.LOW.TabIndex = 3;
            this.LOW.Text = "LOW";
            this.LOW.Click += new System.EventHandler(this.label6_Click);
            // 
            // gb_colors
            // 
            this.gb_colors.Controls.Add(this.label1);
            this.gb_colors.Controls.Add(this.HIGH);
            this.gb_colors.Controls.Add(this.label2);
            this.gb_colors.Controls.Add(this.MED);
            this.gb_colors.Controls.Add(this.label3);
            this.gb_colors.Controls.Add(this.LOW);
            this.gb_colors.Location = new System.Drawing.Point(12, 12);
            this.gb_colors.Name = "gb_colors";
            this.gb_colors.Size = new System.Drawing.Size(117, 113);
            this.gb_colors.TabIndex = 6;
            this.gb_colors.TabStop = false;
            this.gb_colors.Text = "Farben anpassen";
            // 
            // btn_ok
            // 
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_ok.Location = new System.Drawing.Point(12, 131);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(51, 23);
            this.btn_ok.TabIndex = 7;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(69, 131);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Abbruch";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nb_daystogo);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.nb_minutes);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cb_annoyme);
            this.groupBox2.Location = new System.Drawing.Point(135, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(161, 113);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wenn Fristen ablaufen...";
            // 
            // cb_annoyme
            // 
            this.cb_annoyme.AutoSize = true;
            this.cb_annoyme.Location = new System.Drawing.Point(6, 19);
            this.cb_annoyme.Name = "cb_annoyme";
            this.cb_annoyme.Size = new System.Drawing.Size(127, 17);
            this.cb_annoyme.TabIndex = 0;
            this.cb_annoyme.Text = "...dann nerv mich alle";
            this.cb_annoyme.UseVisualStyleBackColor = true;
            // 
            // nb_minutes
            // 
            this.nb_minutes.Location = new System.Drawing.Point(6, 39);
            this.nb_minutes.Name = "nb_minutes";
            this.nb_minutes.Size = new System.Drawing.Size(39, 20);
            this.nb_minutes.TabIndex = 1;
            this.nb_minutes.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Minuten!";
            // 
            // nb_daystogo
            // 
            this.nb_daystogo.Location = new System.Drawing.Point(6, 65);
            this.nb_daystogo.Name = "nb_daystogo";
            this.nb_daystogo.Size = new System.Drawing.Size(39, 20);
            this.nb_daystogo.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tage vor Ablauf";
            // 
            // dlg_settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 165);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.gb_colors);
            this.Name = "dlg_settings";
            this.Text = "Einstellungen";
            this.Load += new System.EventHandler(this.dlg_settings_Load);
            this.gb_colors.ResumeLayout(false);
            this.gb_colors.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_daystogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label HIGH;
        private System.Windows.Forms.Label MED;
        private System.Windows.Forms.Label LOW;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox gb_colors;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nb_minutes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cb_annoyme;
        private System.Windows.Forms.NumericUpDown nb_daystogo;
        private System.Windows.Forms.Label label4;
    }
}