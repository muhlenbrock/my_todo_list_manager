﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace MyTodo
{
    class ThingToDo
    {
        public bool Done = false;
        public string Topic = string.Empty;
        public string Comment = string.Empty;
        public DateTime DoBefore { get; set; }
        public int Prio { get; set; }
        public bool ToBeDeleted = false;
        private bool annoys = true;

        public ThingToDo()
        {
            Prio = ToDoPrio.LOW;
        }

        public void Represent(ListView box)
        {
            if (!ToBeDeleted)
            {
                ListViewItem item = new ListViewItem();
                item.Checked = Done;
                item.SubItems[0].Text = Done ? "Ja" : "Nein";
                item.SubItems.Add(Topic);
                switch (Prio)
                {
                    case 0:
                        item.SubItems.Add("LOW");
                        item.ForeColor = Settings.Set.Col_LOW;
                        break;
                    case 1:
                        item.SubItems.Add("MED");
                        item.ForeColor = Settings.Set.Col_MED;
                        break;
                    case 2:
                        item.SubItems.Add("HIGH");
                        item.ForeColor = Settings.Set.Col_High;
                        break;
                    default: break;
                }                
                item.SubItems.Add(DoBefore.ToShortDateString());
                item.Tag = this;
                item.BackColor = DoBefore < DateTime.Today.AddDays(Settings.Set.AnnoyDeadline) ? Color.Yellow : Color.White;
                box.Items.Add(item);
            }
        }

        public void Delete()
        {
            ToBeDeleted = true;
        }

        public void Rescue()
        {
            ToBeDeleted = false;
        }

        public void ShutUp()
        {
            annoys = false;
        }

        public bool WillAnnoy()
        {
            return annoys && !Done;
        }

    }

    static class ToDoPrio
    {
        public const int LOW = 0;
        public const int MED = 1;
        public const int HIGH = 2;
    }
}
